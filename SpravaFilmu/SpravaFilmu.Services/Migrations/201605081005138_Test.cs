namespace SpravaFilmu.Services.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Test : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Genres", "FilmModel_Id", "dbo.Films");
            DropIndex("dbo.Genres", new[] { "FilmModel_Id" });
            CreateTable(
                "dbo.GenreModelFilmModels",
                c => new
                    {
                        GenreModel_Id = c.Guid(nullable: false),
                        FilmModel_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.GenreModel_Id, t.FilmModel_Id })
                .ForeignKey("dbo.Genres", t => t.GenreModel_Id, cascadeDelete: true)
                .ForeignKey("dbo.Films", t => t.FilmModel_Id, cascadeDelete: true)
                .Index(t => t.GenreModel_Id)
                .Index(t => t.FilmModel_Id);
            
            AddColumn("dbo.Films", "PersonModel_Id", c => c.Guid());
            CreateIndex("dbo.Films", "PersonModel_Id");
            AddForeignKey("dbo.Films", "PersonModel_Id", "dbo.Persons", "Id");
            DropColumn("dbo.Genres", "FilmModel_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Genres", "FilmModel_Id", c => c.Guid());
            DropForeignKey("dbo.GenreModelFilmModels", "FilmModel_Id", "dbo.Films");
            DropForeignKey("dbo.GenreModelFilmModels", "GenreModel_Id", "dbo.Genres");
            DropForeignKey("dbo.Films", "PersonModel_Id", "dbo.Persons");
            DropIndex("dbo.GenreModelFilmModels", new[] { "FilmModel_Id" });
            DropIndex("dbo.GenreModelFilmModels", new[] { "GenreModel_Id" });
            DropIndex("dbo.Films", new[] { "PersonModel_Id" });
            DropColumn("dbo.Films", "PersonModel_Id");
            DropTable("dbo.GenreModelFilmModels");
            CreateIndex("dbo.Genres", "FilmModel_Id");
            AddForeignKey("dbo.Genres", "FilmModel_Id", "dbo.Films", "Id");
        }
    }
}
