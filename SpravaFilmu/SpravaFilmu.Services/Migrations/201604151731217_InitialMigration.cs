namespace SpravaFilmu.Services.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Films",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PhotoPath = c.String(),
                        NameOrg = c.String(),
                        NameCs = c.String(),
                        Runtime = c.Int(nullable: false),
                        Description = c.String(),
                        Country_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.Country_Id)
                .Index(t => t.Country_Id);
            
            CreateTable(
                "dbo.Persons",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        BirthDate = c.DateTime(nullable: false),
                        DeathDate = c.DateTime(nullable: false),
                        PhotoPath = c.String(),
                        FilmModel_Id = c.Guid(),
                        FilmModel_Id1 = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Films", t => t.FilmModel_Id)
                .ForeignKey("dbo.Films", t => t.FilmModel_Id1)
                .Index(t => t.FilmModel_Id)
                .Index(t => t.FilmModel_Id1);
            
            CreateTable(
                "dbo.Genres",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        FilmModel_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Films", t => t.FilmModel_Id)
                .Index(t => t.FilmModel_Id);
            
            CreateTable(
                "dbo.Ratings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Author = c.String(),
                        Rate = c.Int(nullable: false),
                        Text = c.String(),
                        FilmModel_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Films", t => t.FilmModel_Id)
                .Index(t => t.FilmModel_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Ratings", "FilmModel_Id", "dbo.Films");
            DropForeignKey("dbo.Genres", "FilmModel_Id", "dbo.Films");
            DropForeignKey("dbo.Persons", "FilmModel_Id1", "dbo.Films");
            DropForeignKey("dbo.Films", "Country_Id", "dbo.Countries");
            DropForeignKey("dbo.Persons", "FilmModel_Id", "dbo.Films");
            DropIndex("dbo.Ratings", new[] { "FilmModel_Id" });
            DropIndex("dbo.Genres", new[] { "FilmModel_Id" });
            DropIndex("dbo.Persons", new[] { "FilmModel_Id1" });
            DropIndex("dbo.Persons", new[] { "FilmModel_Id" });
            DropIndex("dbo.Films", new[] { "Country_Id" });
            DropTable("dbo.Ratings");
            DropTable("dbo.Genres");
            DropTable("dbo.Persons");
            DropTable("dbo.Films");
            DropTable("dbo.Countries");
        }
    }
}
