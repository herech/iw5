namespace SpravaFilmu.Services.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Foreignkeys : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Films", "PersonModel_Id", "dbo.Persons");
            DropForeignKey("dbo.Persons", "FilmModel_Id", "dbo.Films");
            DropForeignKey("dbo.Persons", "FilmModel_Id1", "dbo.Films");
            DropIndex("dbo.Films", new[] { "PersonModel_Id" });
            DropIndex("dbo.Persons", new[] { "FilmModel_Id" });
            DropIndex("dbo.Persons", new[] { "FilmModel_Id1" });
            CreateTable(
                "dbo.PersonModelFilmModels",
                c => new
                    {
                        PersonModel_Id = c.Guid(nullable: false),
                        FilmModel_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.PersonModel_Id, t.FilmModel_Id })
                .ForeignKey("dbo.Persons", t => t.PersonModel_Id, cascadeDelete: true)
                .ForeignKey("dbo.Films", t => t.FilmModel_Id, cascadeDelete: true)
                .Index(t => t.PersonModel_Id)
                .Index(t => t.FilmModel_Id);
            
            CreateTable(
                "dbo.PersonModelFilmModel1",
                c => new
                    {
                        PersonModel_Id = c.Guid(nullable: false),
                        FilmModel_Id = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.PersonModel_Id, t.FilmModel_Id })
                .ForeignKey("dbo.Persons", t => t.PersonModel_Id, cascadeDelete: true)
                .ForeignKey("dbo.Films", t => t.FilmModel_Id, cascadeDelete: true)
                .Index(t => t.PersonModel_Id)
                .Index(t => t.FilmModel_Id);
            
            DropColumn("dbo.Films", "PersonModel_Id");
            DropColumn("dbo.Persons", "FilmModel_Id");
            DropColumn("dbo.Persons", "FilmModel_Id1");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Persons", "FilmModel_Id1", c => c.Guid());
            AddColumn("dbo.Persons", "FilmModel_Id", c => c.Guid());
            AddColumn("dbo.Films", "PersonModel_Id", c => c.Guid());
            DropForeignKey("dbo.PersonModelFilmModel1", "FilmModel_Id", "dbo.Films");
            DropForeignKey("dbo.PersonModelFilmModel1", "PersonModel_Id", "dbo.Persons");
            DropForeignKey("dbo.PersonModelFilmModels", "FilmModel_Id", "dbo.Films");
            DropForeignKey("dbo.PersonModelFilmModels", "PersonModel_Id", "dbo.Persons");
            DropIndex("dbo.PersonModelFilmModel1", new[] { "FilmModel_Id" });
            DropIndex("dbo.PersonModelFilmModel1", new[] { "PersonModel_Id" });
            DropIndex("dbo.PersonModelFilmModels", new[] { "FilmModel_Id" });
            DropIndex("dbo.PersonModelFilmModels", new[] { "PersonModel_Id" });
            DropTable("dbo.PersonModelFilmModel1");
            DropTable("dbo.PersonModelFilmModels");
            CreateIndex("dbo.Persons", "FilmModel_Id1");
            CreateIndex("dbo.Persons", "FilmModel_Id");
            CreateIndex("dbo.Films", "PersonModel_Id");
            AddForeignKey("dbo.Persons", "FilmModel_Id1", "dbo.Films", "Id");
            AddForeignKey("dbo.Persons", "FilmModel_Id", "dbo.Films", "Id");
            AddForeignKey("dbo.Films", "PersonModel_Id", "dbo.Persons", "Id");
        }
    }
}
