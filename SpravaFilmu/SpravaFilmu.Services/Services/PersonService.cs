﻿using System;

using SpravaFilmu.Model;
using SpravaFilmu.Services.Services.Repository;

namespace SpravaFilmu.Services.Services
{
    public class PersonService : Repository<SpravaFilmuDbContext, PersonModel>
    {
        public PersonService(SpravaFilmuDbContext context) : base(context)
        {
        }
    }
}
