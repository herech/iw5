﻿using System;

using SpravaFilmu.Model;
using SpravaFilmu.Services.Services.Repository;

namespace SpravaFilmu.Services.Services
{
    public class RatingService : Repository<SpravaFilmuDbContext, RatingModel>
    {
        public RatingService(SpravaFilmuDbContext context) : base(context)
        {
        }
    }
}
