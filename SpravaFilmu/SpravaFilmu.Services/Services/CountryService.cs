﻿using System;

using SpravaFilmu.Model;
using SpravaFilmu.Services.Services.Repository;

namespace SpravaFilmu.Services.Services
{
    public class CountryService : Repository<SpravaFilmuDbContext, CountryModel>
    {
        public CountryService(SpravaFilmuDbContext context) : base(context)
        {
        }
    }
}