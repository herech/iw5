﻿using System;

using SpravaFilmu.Model;
using SpravaFilmu.Services.Services.Repository;

namespace SpravaFilmu.Services.Services
{
    public class GenreService : Repository<SpravaFilmuDbContext, GenreModel>
    {
        public GenreService(SpravaFilmuDbContext context) : base(context)
        {
        }
    }
}
