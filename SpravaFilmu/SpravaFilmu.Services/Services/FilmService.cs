﻿using System;
using System.Data.Entity;
using System.Linq;
using SpravaFilmu.Model;
using SpravaFilmu.Services.Services.Repository;

namespace SpravaFilmu.Services.Services
{
    public class FilmService : Repository<SpravaFilmuDbContext, FilmModel>
    {
        public FilmService(SpravaFilmuDbContext context) : base(context)
        {

        }

        public FilmModel GetFullById(Guid id)
        {
            return this.Context.Films.Where(f => f.Id == id).Include(f => f.Actors).Include(f => f.Directors).Include(f => f.Genres).Include(f => f.Ratings).FirstOrDefault();
        }
    }
}