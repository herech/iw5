﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpravaFilmu.Model;

namespace SpravaFilmu.Services
{
    public class SpravaFilmuDbContext : DbContext
    {
        public DbSet<PersonModel> Persons { get; set; }
        public DbSet<CountryModel> Countries { get; set; }
        public DbSet<GenreModel> Genres { get; set; }
        public DbSet<FilmModel> Films { get; set; }
        public DbSet<RatingModel> Ratings { get; set; }

        //TODO change connection string before using database
        public SpravaFilmuDbContext()
            : base(@"Data Source=(LocalDB)\sqlServer;Persist Security Info=True;MultipleActiveResultSets=True;Integrated Security=SSPI")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<SpravaFilmuDbContext, SpravaFilmuDbContextDbMigrationsConfiguration<SpravaFilmuDbContext>>());
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<PersonModel>().ToTable("Persons");
            modelBuilder.Entity<CountryModel>().ToTable("Countries");
            modelBuilder.Entity<GenreModel>().ToTable("Genres");
            modelBuilder.Entity<FilmModel>().ToTable("Films");
            modelBuilder.Entity<RatingModel>().ToTable("Ratings");
            //modelBuilder.Entity<TaskModel>().HasOptional(t => t.ParentTask).WithMany(t => t.Tasks);
        }
    }
}
