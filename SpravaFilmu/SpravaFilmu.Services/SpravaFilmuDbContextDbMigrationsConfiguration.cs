﻿using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace SpravaFilmu.Services
{
    public class SpravaFilmuDbContextDbMigrationsConfiguration<T> : DbMigrationsConfiguration<T>
        where T : DbContext
    {
        public SpravaFilmuDbContextDbMigrationsConfiguration()
        {
            this.AutomaticMigrationsEnabled = true;
            this.AutomaticMigrationDataLossAllowed = true;
        }
    }
}