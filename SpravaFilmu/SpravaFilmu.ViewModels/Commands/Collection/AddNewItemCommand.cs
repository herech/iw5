﻿using SpravaFilmu.ViewModels.Framework.Commands;
using SpravaFilmu.ViewModels.Framework.ViewModels;
using SpravaFilmu.Model;

namespace SpravaFilmu.ViewModels.Commands.Collection
{
    public class AddNewItemCommand<T> : CommandBase<ViewModelCollection<T>> where T : BaseModel, new()
    {
        public AddNewItemCommand(ViewModelCollection<T> viewModelCollection)
            : base(viewModelCollection)
        {
        }

        public override void Execute(object item)
        {
            this.ViewModel.Service.Add(this.ViewModel.NewItem);
            this.ViewModel.Items.Add(this.ViewModel.NewItem);
            this.ViewModel.NewItem = new T();
        }
    }
}