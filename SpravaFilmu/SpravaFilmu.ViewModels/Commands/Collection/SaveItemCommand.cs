﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpravaFilmu.Model;
using SpravaFilmu.ViewModels.Framework.Commands;
using SpravaFilmu.ViewModels.Framework.ViewModels;

namespace SpravaFilmu.ViewModels.Commands.Collection
{
    class SaveItemCommand<T> : CommandBase<ViewModelCollection<T>> where T : BaseModel, new()
    {
        public SaveItemCommand(ViewModelCollection<T> viewModelCollection) : base(viewModelCollection)
        {}

        public override void Execute(object parameter)
        {
            this.ViewModel.Service.Add(this.ViewModel.NewItem);
            this.ViewModel.Items.Add(this.ViewModel.NewItem);
            this.ViewModel.SaveData();
            this.ViewModel.NewItem = new T();
        }
    }
}
