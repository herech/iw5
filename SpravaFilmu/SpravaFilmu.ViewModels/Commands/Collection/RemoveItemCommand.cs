﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpravaFilmu.Model;
using SpravaFilmu.ViewModels.Framework.Commands;
using SpravaFilmu.ViewModels.Framework.ViewModels;

namespace SpravaFilmu.ViewModels.Commands.Collection
{
    public class RemoveItemCommand<T> : CommandBase<ViewModelCollection<T>> where T : BaseModel, new()
    {
        public RemoveItemCommand(ViewModelCollection<T> viewModelCollection) : base(viewModelCollection)
        {}

        public override void Execute(object parameter)
        {
            var typedItem = parameter as T;
            if (typedItem != null)
            {
                this.ViewModel.Items.Remove(typedItem);
            }
        }
    }
}
