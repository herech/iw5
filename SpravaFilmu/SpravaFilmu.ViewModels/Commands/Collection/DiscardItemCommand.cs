﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpravaFilmu.Model;
using SpravaFilmu.ViewModels.Framework.Commands;
using SpravaFilmu.ViewModels.Framework.ViewModels;

namespace SpravaFilmu.ViewModels.Commands.Collection
{
    public class DiscardItemCommand<T> : CommandBase<ViewModelCollection<T>> where T : BaseModel, new()
    {
        public DiscardItemCommand(ViewModelCollection<T> viewModelCollection) : base(viewModelCollection)
        {}

        public override void Execute(object parameter)
        {
            this.ViewModel.NewItem = null;
        }
    }
}
