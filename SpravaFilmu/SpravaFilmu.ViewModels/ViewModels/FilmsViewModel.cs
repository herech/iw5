﻿using System;
using System.Windows.Input;
using SpravaFilmu.Model;
using SpravaFilmu.Services;
using SpravaFilmu.Services.Services;
using SpravaFilmu.ViewModels.Commands.Collection;
using SpravaFilmu.ViewModels.Framework.ViewModels;

namespace SpravaFilmu.ViewModels.ViewModels
{
    public class FilmsViewModel : ViewModelCollection<FilmModel>
    {
        public FilmsViewModel(SpravaFilmuDbContext context) : base()
        {
            this.Service = new FilmService(context);
            base.LoadData();
            this.NewItem = new FilmModel();
        }

        public FilmModel getById(Guid id)
        {
            FilmService service = (FilmService) this.Service;
            return service.GetFullById(id);
        }
    }
}