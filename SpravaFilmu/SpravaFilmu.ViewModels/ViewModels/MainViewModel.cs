﻿using System;

using SpravaFilmu.Model;
using SpravaFilmu.Services;
using SpravaFilmu.ViewModels.Framework.ViewModels;

namespace SpravaFilmu.ViewModels.ViewModels
{
    public class MainViewModel : ViewModelBase<BaseModel>
    {
        public CountriesViewModel CountriesViewModel { get; set; }
        public FilmsViewModel FilmsViewModel { get; set; }
        public GenresViewModel GenresViewModel { get; set; }
        public PersonsViewModel PersonsViewModel { get; set; }
        public RatingsViewModel RatingsViewModel { get; set; }

        private SpravaFilmuDbContext _context;

        public MainViewModel()
        {
            this._context = new SpravaFilmuDbContext();
            this.CountriesViewModel = new CountriesViewModel(this._context);
            this.FilmsViewModel = new FilmsViewModel(this._context);
            this.GenresViewModel = new GenresViewModel(this._context);
            this.PersonsViewModel = new PersonsViewModel(this._context);
            this.RatingsViewModel = new RatingsViewModel(this._context);
        }

        public override void LoadData()
        {
            this.CountriesViewModel.LoadData();
            this.FilmsViewModel.LoadData();
            this.GenresViewModel.LoadData();
            this.PersonsViewModel.LoadData();
            this.RatingsViewModel.LoadData();
        }


        public override void OnProgramShutdownStarted(object sender, EventArgs e)
        {
            this.CountriesViewModel.SaveData();
            this.FilmsViewModel.SaveData();
            this.GenresViewModel.SaveData();
            this.PersonsViewModel.SaveData();
            this.RatingsViewModel.SaveData();

            this.CountriesViewModel.Dispose();
            this.FilmsViewModel.Dispose();
            this.GenresViewModel.Dispose();
            this.PersonsViewModel.Dispose();
            this.RatingsViewModel.Dispose();
        }
    }
}
