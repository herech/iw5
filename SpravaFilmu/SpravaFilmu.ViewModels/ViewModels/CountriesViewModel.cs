﻿using System.Windows.Input;
using SpravaFilmu.Model;
using SpravaFilmu.Services;
using SpravaFilmu.Services.Services;
using SpravaFilmu.ViewModels.Commands.Collection;
using SpravaFilmu.ViewModels.Framework.ViewModels;

namespace SpravaFilmu.ViewModels.ViewModels
{
    public class CountriesViewModel : ViewModelCollection<CountryModel>
    {
        public CountriesViewModel(SpravaFilmuDbContext contex) : base()
        {
            this.Service = new CountryService(contex);
            base.LoadData();
        }
    }
}