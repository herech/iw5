﻿using System.Windows.Input;
using SpravaFilmu.Model;
using SpravaFilmu.Services;
using SpravaFilmu.Services.Services;
using SpravaFilmu.ViewModels.Commands.Collection;
using SpravaFilmu.ViewModels.Framework.ViewModels;

namespace SpravaFilmu.ViewModels.ViewModels
{
    public class PersonsViewModel : ViewModelCollection<PersonModel>
    {
        public PersonsViewModel(SpravaFilmuDbContext context) : base()
        {
            this.Service = new PersonService(context);
            base.LoadData();
        }
    }
}