﻿using System.Windows.Input;
using SpravaFilmu.Model;
using SpravaFilmu.Services;
using SpravaFilmu.Services.Services;
using SpravaFilmu.ViewModels.Commands.Collection;
using SpravaFilmu.ViewModels.Framework.ViewModels;

namespace SpravaFilmu.ViewModels.ViewModels
{
    public class RatingsViewModel : ViewModelCollection<RatingModel>
    {
        public RatingsViewModel(SpravaFilmuDbContext context) : base()
        {
            this.Service = new RatingService(context);
            base.LoadData();
        }
    }
}