﻿using System.Windows.Input;
using SpravaFilmu.Model;
using SpravaFilmu.Services;
using SpravaFilmu.Services.Services;
using SpravaFilmu.ViewModels.Commands.Collection;
using SpravaFilmu.ViewModels.Framework.ViewModels;

namespace SpravaFilmu.ViewModels.ViewModels
{
    public class GenresViewModel : ViewModelCollection<GenreModel>
    {
        public GenresViewModel(SpravaFilmuDbContext context) : base()
        {
            this.Service = new GenreService(context);
            base.LoadData();
        }
    }
}