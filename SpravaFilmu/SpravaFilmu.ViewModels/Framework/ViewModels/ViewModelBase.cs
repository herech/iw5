﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpravaFilmu.Model;
using SpravaFilmu.Services.Services.Repository;
using SpravaFilmu.Services;
using System.Runtime.CompilerServices;

namespace SpravaFilmu.ViewModels.Framework.ViewModels
{
    public abstract class ViewModelBase<T> : INotifyPropertyChanged, IDisposable where T : BaseModel
    {
        private bool _disposed;

        public Repository<SpravaFilmuDbContext, T> Service { get; protected set; }


        public event PropertyChangedEventHandler PropertyChanged;

        ~ViewModelBase()
        {
            this.Dispose(false);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        public abstract void LoadData();

        public void SaveData()
        {
            this.Service.Save();
        }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        private void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    this.Service.Dispose();
                }
                this._disposed = true;
            }
        }

        public virtual void OnProgramShutdownStarted(object sender, EventArgs e)
        {
            this.Service.Save();
            this.Dispose();
        }
    }
}

