﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using SpravaFilmu.Model;
using SpravaFilmu.ViewModels.Commands.Collection;

namespace SpravaFilmu.ViewModels.Framework.ViewModels
{
    public abstract class ViewModelCollection<T> : ViewModelBase<T> where T : BaseModel, new()
    {
        private T _newItem;

        public ICommand SaveNewItem { get; set; }
        public ICommand DiscardNewItem { get; set; }
        public ICommand RemoveItem { get; set; }
        public ICommand AddNewItem { get; set; }

        public ObservableCollection<T> Items
        {
            get { return this.Service.GetObservableCollection(); }
        }

        public T NewItem
        {
            get { return _newItem; }
            set
            {
                if (Equals(value, _newItem))
                {
                    return;
                }
                _newItem = value;
                OnPropertyChanged();
            }
        }

        protected ViewModelCollection()
        {
            this.RemoveItem = new RemoveItemCommand<T>(this);
            this.SaveNewItem = new SaveItemCommand<T>(this);
            this.DiscardNewItem = new DiscardItemCommand<T>(this);
            this.AddNewItem = new AddNewItemCommand<T>(this);
            this.NewItem = new T();
        }

        public override async void LoadData()
        {
            await Task.Delay(1000);
            this.Service.LoadAll();
            OnPropertyChanged(nameof(Items));
        }

    }
}
