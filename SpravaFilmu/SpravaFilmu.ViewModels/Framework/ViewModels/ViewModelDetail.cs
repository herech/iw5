﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpravaFilmu.Model;

namespace SpravaFilmu.ViewModels.Framework.ViewModels
{
    public abstract class ViewModelDetail<T> : ViewModelBase<T> where T : BaseModel
    {
        protected ViewModelDetail(T item)
        {
            this.Item = item;
        }

        public T Item { get; private set; }

        public override void LoadData()
        {
            Item = Service.GetByID(Item.Id);
        }

    }
}
