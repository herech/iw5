﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SpravaFilmu.Model;
using SpravaFilmu.ViewModels.ViewModels;

namespace SpravaFilmu.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            this.InitializeComponent();
        }

        private void AddFilmButtonClicked(object sender, RoutedEventArgs e)
        {
            var addFilmWindow = new AddFilmWindow();
            addFilmWindow.Show();
        }

        private void AddItemButtonClicked(object sender, RoutedEventArgs e)
        {
            var addOthersWindow = new AddOthersWindow();
            addOthersWindow.Show();
        }

        private void FilmListBoxDoubleClicked(object sender, MouseButtonEventArgs mouseButtonEventArgs)
        {
            var viewFilmDetailWindow = new ViewFilmDetailWindow();
            MainViewModel mainViewModel = (MainViewModel) this.FindResource("MainViewModel");
            FilmsViewModel filmsViewModel = mainViewModel.FilmsViewModel;

            ListBox listbox = (ListBox) sender;
            FilmModel selected = (FilmModel) listbox.SelectedItem;

            
            viewFilmDetailWindow.DataContext = filmsViewModel.getById(selected.Id);
            viewFilmDetailWindow.Show();
        }
    }
}
