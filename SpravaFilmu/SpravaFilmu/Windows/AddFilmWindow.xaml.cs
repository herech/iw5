﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using SpravaFilmu.Model;
using SpravaFilmu.ViewModels.ViewModels;

namespace SpravaFilmu.Windows
{
    /// <summary>
    /// Interaction logic for AddFilmWindow.xaml
    /// </summary>
    public partial class AddFilmWindow : Window
    {
        public AddFilmWindow()
        {
            InitializeComponent();
        }

        private void SaveFilmButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainViewModel mainModel = (MainViewModel) this.FindResource("MainViewModel");
            FilmsViewModel viewModel = mainModel.FilmsViewModel;
            foreach (PersonModel person in this.PersonSelect.SelectedItems)
            {
                viewModel.NewItem.AddActor(person);
            }
            foreach (PersonModel person in this.DirectorSelect.SelectedItems)
            {
                viewModel.NewItem.AddDirector(person);
            }
            foreach (GenreModel person in this.GenreSelect.SelectedItems)
            {
                viewModel.NewItem.AddGenre(person);
            }

            CountryModel selectedCountry = (CountryModel) this.CountrySelect.SelectedItem;

            viewModel.NewItem.CountryID = selectedCountry.Id;
            viewModel.SaveNewItem.Execute(null);
            this.Close();
        }
    }
}
