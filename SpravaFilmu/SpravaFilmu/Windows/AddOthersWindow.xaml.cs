﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SpravaFilmu.Model;
using SpravaFilmu.ViewModels.ViewModels;

namespace SpravaFilmu.Windows
{
    /// <summary>
    /// Interaction logic for AddWindow.xaml
    /// </summary>
    public partial class AddOthersWindow : Window
    {
        public AddOthersWindow()
        {
            this.InitializeComponent();
        }

        private void SaveRatingItemButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainViewModel mainModel = (MainViewModel)this.FindResource("MainViewModel");
            RatingsViewModel viewModel = mainModel.RatingsViewModel;

            FilmModel selectedFilm = (FilmModel)this.FilmSelect.SelectedItem;
            viewModel.NewItem.Rate = this.RatingSelect.SelectedIndex;

            viewModel.NewItem.Film = selectedFilm;
            viewModel.SaveNewItem.Execute(null);
        }
    }
}
