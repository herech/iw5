﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpravaFilmu.Model
{
    public class GenreModel : BaseModel
    {

        public GenreModel(Guid id) : base(id) {

        }
 
        public GenreModel() : base()
        {
        }


        public string Name { get; set; }

        public virtual ObservableCollection<FilmModel> Films { get; set; } 
    }
}
