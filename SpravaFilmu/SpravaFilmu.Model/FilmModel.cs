﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SpravaFilmu.Model
{
    public class FilmModel : BaseModel
    {
        public FilmModel() : base()
        {
           InitColllections();
        }

        public FilmModel(Guid id) : base(id) { 
           InitColllections();
        }

        public void InitColllections()
        {
            Genres = new ObservableCollection<GenreModel>();
            Actors = new ObservableCollection<PersonModel>();
            Directors = new ObservableCollection<PersonModel>();
            Ratings = new ObservableCollection<RatingModel>();
        }

        private int _runtime;

        public virtual ObservableCollection<GenreModel> Genres { get; set; }
        public virtual ObservableCollection<PersonModel> Actors { get; set; }
        public virtual ObservableCollection<PersonModel> Directors { get; set; }
        public virtual ObservableCollection<RatingModel> Ratings { get; private set; }  

        public virtual CountryModel Country { get; set; }
        public Guid CountryID { get; set; }

        public string PhotoPath { get; set; }
        public string NameOrg { get; set; }
        public string NameCs { get; set; }

        public string Name => $"{NameOrg} ({NameCs})";

        public int Runtime
        {
            get
            {
                return this._runtime;
            }
            set
            {
                if (value > 0)
                {
                    this._runtime = value;
                }
            }
        }
        public string Description { get; set; }

        public int AvgRating => GetAverageRating();
        public int GetAverageRating()
        {
            if (this.Ratings.Any())
            {
                return this.Ratings.Sum(rate => (rate.Rate*20)) / this.Ratings.Count;
            }
            return 0;
        }

        public void AddRating(RatingModel rating)
        {
            if (rating != null) this.Ratings.Add(rating);
        }

        public void AddRating(int rate, String text, String author = null)
        {
            RatingModel rating = new RatingModel
            {
                Rate = rate,
                Text = text,
                Author = author
            };
            this.Ratings.Add(rating);
        }

        public void AddGenre(GenreModel genre)
        {
            if (genre != null && !(this.Genres.Any(g => g.Id == genre.Id)))
            {
                this.Genres.Add(genre);
            }
        }

        public void AddGenre(String name)
        {
            GenreModel genre = new GenreModel
            {
                Name = name
            };
            this.Genres.Add(genre);
        }

        public void AddActor(PersonModel person)
        {
            if (person != null)
            {
                if (Actors.All(p => p.Id != person.Id))
                {
                    this.Actors.Add(person);
                }
            }
        }

        public void AddActor(String firstName, String lastName, DateTime birthDate, String photoPath, DateTime deathDate = default(DateTime))
        {
            PersonModel actor = new PersonModel
            {
                FirstName = firstName,
                LastName = lastName,
                BirthDate = birthDate,
                PhotoPath = photoPath,
                DeathDate = deathDate

            };
            this.Actors.Add(actor);
        }

        public void AddDirector(PersonModel person)
        {
            if (person != null && !(this.Directors.Any(a => a.Id == person.Id)))
            {
                this.Directors.Add(person);
            }
        }

        public void AddDirector(String firstName, String lastName, DateTime birthDate, String photoPath, DateTime deathDate = default(DateTime))
        {
            PersonModel director = new PersonModel
            {
                FirstName = firstName,
                LastName = lastName,
                BirthDate = birthDate,
                PhotoPath = photoPath,
                DeathDate = deathDate

            };
            this.Actors.Add(director);
        }


    }
}
