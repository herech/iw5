﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpravaFilmu.Model
{
    public class PersonModel : BaseModel
    {
        public PersonModel(Guid id) : base(id) {

        }

        public PersonModel() : base()
        {
        }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Name => $"{FirstName} {LastName}";

        [InverseProperty("Actors")]
        public virtual ObservableCollection<FilmModel> FilmsActors { get; set; }

        [InverseProperty("Directors")]
        public virtual ObservableCollection<FilmModel> FilmsDirectors { get; set; } 

        private DateTime _birthDate;
        public DateTime BirthDate
        {
            get { return _birthDate; }
            set
            {
                if (DateTime.Now.Date > value.Date)
                    this._birthDate = value;
            }
        }

        public DateTime DeathDate { get; set; }

        public string PhotoPath { get; set; }

        public int GetAge()
        {
            DateTime todayDate = DateTime.Today;
            if (this.DeathDate != default(DateTime))
            {
                double days = (todayDate.Date - this.BirthDate.Date).TotalDays;
                return Convert.ToInt32(days / 365.25);
            }
            else
            {
                double days = (this.DeathDate.Date - this.BirthDate.Date).TotalDays;
                return Convert.ToInt32(days / 365.25);
            }

        }
    }
}
