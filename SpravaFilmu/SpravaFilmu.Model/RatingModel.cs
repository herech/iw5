﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpravaFilmu.Model
{
    public class RatingModel : BaseModel
    {

        public RatingModel(Guid id) : base(id) {

        }

        public RatingModel() : base()
        {
        }


        public string Author { get; set; }

        private int _rate;

        public int Rate {
            get { return this._rate; }
            set
            {
                if (value >= 0 && value < 6)
                    this._rate = value;
            }
        }
        public string Text { get; set; }

        public virtual FilmModel Film { get; set; }
    }
}
