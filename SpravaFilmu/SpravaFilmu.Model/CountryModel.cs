﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace SpravaFilmu.Model
{
    public class CountryModel : BaseModel
    {
        public CountryModel() : base()
        {
            
        }

        public CountryModel(Guid id) : base(id) {

        }

        public String Name { get; set; }

    }
}
